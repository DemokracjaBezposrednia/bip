## Zarząd Krajowy
* Skarbnik - Janusz Lewandowski, skarbnik@db.org.pl, +48 664 033 810
* Członek Zarządu - Artur Lorek, artur.lorek@db.org.pl, +48 600 728 242
* Członek Zarządu - Dorota Radziszewska, dorota.radziszewska@db.org.pl

## Komisja Rewizyjna
* Tomasz Butrym

## Zespoły zadaniowe
* [[Zespół wyborczy]]
* [[Zespół zadaniowy ds Przedsiębiorców i Gospodarki]]
