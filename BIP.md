# Biuletyn Informacji Publicznej
Witamy na stronach Biuletynu Informacji Publicznej Demokracji Bezpośredniej.

Biuletyn Informacji Publicznej to serwis internetowy stworzony w celu powszechnego udostępniania informacji publicznej, wymagany przez ustawę o dostępie do informacji publicznej.

Główny serwis Demokracji Bezpośredniej znajduje się pod adresem http://db.org.pl.

## Dostępne informacje
* [[Dane adresowe]]
* [[Statut]]
* [[Regulamin oświadczeń]]
* [[Składki]] członkowskie: [[Regulamin składek i darowizn]]
* [[Organy krajowe]]
* [[Program - Turkusowa (R)ewolucja|http://db.org.pl/program/]]
* [[WZC - dokumenty|WZC-dokumenty]]
* [[Zarząd DB - uchwały|Zarzad DB-uchwaly]]
* [[Sprawozdania finansowe]]
* [[Rejestr członków DB|https://docs.google.com/spreadsheets/d/1lcrHfNlk9FjPEng7DVULGhPRxJQhY-bevT-0Mfiv7rk/]]
* [[Pliki do pobrania - wzory dokumentów|Pliki-do-pobrania]]
* [[Identyfikacja wizualna]]
