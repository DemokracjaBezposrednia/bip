# Protokół ze Zjazdu Wyborczego IX Walnego Zebrania Członków Demokracji Bezpośredniej w Sulejowie w dniu 27.06.2015 r.

Zjazd Wyborczy Walnego Zgromadzenia Członków (dalej zwany WZ) partii Demokracja Bezpośrednia (zwana dalej DB) odbył się dnia 27.06.2013 roku w Sulejowie w sali konferencyjnej Ośrodku Wypoczynkowym "Anpol" Sulejów Polanka.

Posiedzenie Walnego Zgromadzenia wyznaczył zarząd w drodze uchwały zarządu, a charakter Zjazdu Wyborczego uzyskało na mocy postanowienia przewodniczącego z dnia 10.06.2015. (załącznik nr 2). Wybory zostały zaplanowane w związku z zaplanowaną przez przewodniczącego zarządu Adama Kotuchy rezygnacją (dokonana 11.06.2015) oraz rezygnacją z funkcji wiceprzewodniczącego Dominika Hernika. Potrzebna była również rekonstrukcja osobowa komisji rewizyjnej z uwagi na fakt, iż dwóch jej członków zrezygnowało z członkostwa w partii: Filip Rembiałkowski i Andrzej Gorayski, a wybór trzeciego - Moniki Włodarczyk został uznany za nieważny z uwagi na powiązania rodzinne z członkami zarządu, co było wbrew statutowi. WZ zwołała drogą elektroniczną sekretarz Beata Dziudzik rozsyłając do wszystkich maile z zawiadomieniem i porządkiem obrad (załącznik nr 3).

WZ rozpoczęło się o godzinie 15:15.

Ustalono, iż z obecnych członków zarządu nie stawił się nikt z wyjątkiem wiceprzewodniczącej. Nie stawiła się również wbrew zapowiedziom sekretarz Beata Dziudzik, która nie dostarczyła żadnych formularzy używanych zazwyczaj przy WZ oraz kart do głosowania.

Wobec tej sytuacji, WZ wybrało Pawła Tanajno na przewodniczącego obrad w głosowaniu, gdzie 15 osób było ZA, 3 osoby PRZECIW i 8 wstrzymujących się.

Paweł Tanajno poprosił zgromadzonych, aby podpisali się na liście członków DB przygotowaną przez skarbnika Janusza Lewandowskiego (załącznik nr 4).

Na podstawie listy obecności ustalono, iż na godzinę 15:30 przybyło 26 członków, z czego uprawnionych do głosowań w wyborach członków zarządu i komisji rewizyjnej - 22. Do zaplanowanych w porządku obrad głosowań, nie było wymagane kworum, więc Walne rozpoczęło obrady.

## Przebieg WZ
Obrady otworzył przewodniczący zgromadzenia (dalej PZ) Paweł Tanajno. Przyjęto porządek obrad z uwzględnieniem wniosku Piotra Mądrego o dopisanie przed wyborem komisji skrutacyjnej wniosku o przyjęcie do DB decyzją Walnego Tomasza Butryma oraz wniosku PZ, aby wobec nieobecności skarbnika i byłego prezesa, przesunąć pkt. 5 i 6 planowanego porządku obrad na koniec WZ. Porządek obrad ze zmianami został przyjęty 25 głosami ZA i jednym WSTRZYMUJĄCYM. 

Wobec braku formularzy i przygotowanych kart do głosowania przygotowano zastępcze. Przewodniczący wyjaśnił Piotrowi Mądremu, iż zgodnie ze statutem nowych członków może przyjąć tylko zarząd i głosowanie Walnego nad jego wnioskiem o przyjęcie Tomasza Butryma będzie nieskuteczne, nieważne, aczkolwiek wobec woli większości PZ dopuści to głosowanie.

Z użyciem kart zastępczych przegłosowano przyjęcie Tomasza Butryma w grono członków Demokracji Bezpośredniej (26 głosów ZA) oraz wybór na członków komisji skrutacyjnej (25 głosów ZA i 1 głos WSTRZYMUJĄCY SIĘ):
* Tomasza Rockiego
* Tomasza Moronia
* Tomasza Wciślika 

W tym momencie do zebrania dołączyły trzy osoby - Adam Kotucha, Patryk Wołczyński oraz Anna Kołodziejczyk. Adam Kotucha przywiózł formularze i prawidłowe karty do głosowania, a Anna Kołodziejczyk i Tomasz Butrym rozpoczęli zbierania podpisów poparcia pod swoimi kandydaturami na członków zarządu oraz dla kandydatury Michała Broniewicza, który wcześniej online złożył oświadczenie o chęci kandydowania. Komisja skrutacyjna rozpoczęła wydawania poprawnych kart do głosowania, które miały zostać użyte w wyborach, odbierając i kasując tymczasowe.

Na wniosek wiceprzewodniczącej zarządu Agnieszki Pabich-Tanajno ogłoszono chwilę przerwy i zwołano zaoczne posiedzenie zarządu w celu przyjęcia w poczet członków DB Tomasza Butrym. Adam Kotucha pełniąc rolę asystenta wykonał telefony do wszystkich członków zarządu i potwierdził, iż wszyscy zgadzają się na przyjęcie Tomasza Butryma.

Przed rozpoczęciem głosowań PZ oświadczył, iż otrzymał następujące zgłoszenia kandydatur popartą odpowiednią liczbą podpisów:

Na przewodniczącego:
Marzena Petykiewicz, Michał Broniewicz, Tomasz Butrym

Na wiceprzewodniczącego:
Adam Wiadrowski, Anna Kołodziejek

Kandydaci długo i obszernie przedstawili swoje sylwetki i swój program.

Ustalono, że po przybyciu 3 osób, liczba uprawnionych do głosowania wynosi 24 osoby, liczba wszystkich uczestników walnego 29 osób. W trakcie prezentacji kandydatów, przed rozpoczęciem głosowań salę opuściła jedna uprawniona do głosowania osoba.

## Głosowania nad wyborem członków zarządu i komisji rewizyjnej
Komisja skrutacyjna wydała 23 karty do głosowania. W obu głosowaniach do urny wrzucono 22 karty. Głosowania odbywały się systemem wyboru jednego kandydata z listy.

## Uchwała o wyborze przewodniczącego Demokracji Bezpośredniej nr 1/06/2015
Walne Zgromadzenie Członków Demokracji Bezpośredniej, zapełniając wakat po rezygnacji Adama Kotuchy z dnia 11 czerwca 2015, na przewodniczącego wybiera Marzenę Petykiewicz.

Wynik głosowania tajnego: Marzena Petykiewicz uzyskała 14 głosów. Michał Broniewicz otrzymał 1 głos, Tomasz Butrym 7 głosów.

Wobec powyższego nowym przewodniczącym do końca kadencji została Marzena Petykiewicz. Uchwała wchodzi w życie z dniem podjęcia tj. 27.06.2015.

-----

## Uchwała o wyborze wiceprzewodniczącego Demokracji Bezpośredniej nr 2/06/2015
Walne Zgromadzenie Członków Demokracji Bezpośredniej, zapełniając wakat po rezygnacji Dominika Hernika z dnia 15 maja 2015, na wiceprzewodniczącego wybiera Adama Wiadrowskiego.

Wynik głosowania tajnego: Adam Wiadrowski uzyskał 13 głosów. Anna Kołodziejek otrzymała 9 głosów.

Wobec powyższego nowym wiceprzewodniczącym do końca kadencji został Adam Wiadrowski. Uchwała wchodzi w życie z dniem podjęcia tj. 27.06.2015.

-----

Po tym głosowaniu salę opuściła dużą część uczestników. 

Przewodniczący zarządził wybory komisji rewizyjnej. Zgłosili się - Krzysztof Szewczyk, Adam Kotucha, Anna Kołodziejek, Tomasz Butrym.

## Uchwała o wyborze członków komisji rewizyjnej Demokracji Bezpośredniej nr 3/06/2015
Walne Zgromadzenie Członków Demokracji Bezpośredniej, wybiera na członków komisji rewizyjnej: Krzysztofa Szewczyka, Annę Kołodziejek i Tomasza Butryma.

W głosowaniu tajnym Krzysztof Szewczyk otrzymał 15 głosów, Anna Kołodziejek otrzymała 12 głosów, Tomasz Butrym otrzymał 12 głosów, Adam Kotucha otrzymał 7 głosów.

-----

Protokół komisji skrutacyjnej załącznik nr 1.

## Sprawy organizacyjne
Od 18:19 do końca zebrania przewidzianego do godziny 19:00, dyskusja nad sprawami organizacyjnym została zdominowana przez pytania dotyczące finansowania SDD. Wobec nieobecności skarbnika, na pytania odpowiadał głównie Adam Kotucha, który osobiście w czasie kiedy pełnił funkcję członka zarządu, kontrolował tą sprawę. W sprawach dotyczących przygotowań DB do startu w wyborach parlamentarnych 2015 przywołano temat propozycji złożonej DB przez Pawła Kukiza.

0 19:00 w związku z końcem wynajmu sali przewodniczący zamknął obrady. 

Przewodniczący obrad:
Paweł Tanajno